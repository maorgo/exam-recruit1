@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Interview date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "summary">Interview summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div> 

        <select class="form-control" name="candidate_id">
              @foreach($candidates as $candidate)
              <option value="{{ $candidate->id }}">{{ $candidate->name }}</option>
              @endforeach
            </select>
                                         
        <select class="form-control" name="user_id">
              @foreach($users as $user)
              <option value="{{ $user->id }}">{{ $user->name }}</option>
              @endforeach
            </select>
            <input type = "submit" name = "submit" value = "Create interview">
                              
        </form>    
@endsection
