@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Summary</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @if($interviews->count() == 0)
    <tr><td>You have no interviews</td></tr>
    @endif
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->summary}}</td>     
            <td> @if(isset($interview->candidate_id))
                          {{$interview->candidate->name}}  
                        @else
                          No candidate
                        @endif        
                        </td>          
            <td> @if(isset($interview->user_id))
                          {{$interview->user->name}}  
                        @else
                          No user
                        @endif        
                        </td>        
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>                                                           
        </tr>
    @endforeach
</table>
@endsection

