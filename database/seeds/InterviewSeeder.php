<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'date' => Carbon::now(),
            'summary' => Str::random(100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);        
    }
}
